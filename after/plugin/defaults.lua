vim.opt.list = true
vim.opt.tabstop = 2

vim.keymap.set('n', '<leader>t', '<cmd>Neotree toggle<CR>', { desc = 'Toggle File [T]ree Explorer' })

local ok, jdtls = pcall(require, "jdtls")

if not ok then
  return
end

local jdtls_prefix = "<leader>lj"
local home = os.getenv("HOME")

local function jdtls_on_attach(_, buffer)
  require("utils").on_attach(_, buffer)
  vim.keymap.set(
    "n",
    jdtls_prefix .. "i",
    "<Cmd>lua require'jdtls'.organize_imports()<CR>",
    { buffer = buffer, desc = "Organize Imports" }
  )
  vim.keymap.set(
    "n",
    jdtls_prefix .. "t",
    "<Cmd>lua require'jdtls'.test_class()<CR>",
    { buffer = buffer, desc = "Test Class" }
  )
  vim.keymap.set(
    "n",
    jdtls_prefix .. "n",
    "<Cmd>lua require'jdtls'.test_nearest_method()<CR>",
    { buffer = buffer, desc = "Test Nearest Method" }
  )
  vim.keymap.set(
    "v",
    jdtls_prefix .. "e",
    "<Esc><Cmd>lua require('jdtls').extract_variable(true)<CR>",
    { buffer = buffer, desc = "Extract Variable" }
  )
  vim.keymap.set(
    "n",
    jdtls_prefix .. "e",
    "<Cmd>lua require('jdtls').extract_variable()<CR>",
    { buffer = buffer, desc = "Extract Variable" }
  )
  vim.keymap.set(
    "v",
    jdtls_prefix .. "m",
    "<Esc><Cmd>lua require('jdtls').extract_method(true)<CR>",
    { buffer = buffer, desc = "Extract Method" }
  )
  jdtls.setup_dap({ hotcodereplace = 'auto' })
end

local function get_jdtls_bundles()
  local base_dir = home .. "/.local/share/nvim/mason/packages"
  local jar_patterns = {
    'java-debug-adapter/extension/server/com.microsoft.java.debug.plugin-*.jar',
    'java-test/extension/server/*.jar',
  }
  local bundles = {}
  for _, jar_pattern in ipairs(jar_patterns) do
    local full_path = base_dir .. "/" .. jar_pattern
    for _, bundle in ipairs(vim.split(vim.fn.glob(full_path), '\n', {})) do
      if not vim.endswith(bundle, "runner-jar-with-dependencies.jar") then
        table.insert(bundles, bundle)
      end
    end
  end
  return bundles
end

local function java_callback()
  local root_dir = require("jdtls.setup").find_root({ ".git", "mvnw", "gradlew" })
  local project_name = vim.fn.fnamemodify(root_dir, ":p:h:t")
  local workspace_dir = home ..
      "/.local/share/eclipse/workspace/" ..
      project_name -- See `:help vim.lsp.start_client` for an overview of the supported `config` options.
  local config = {
    on_attach = jdtls_on_attach,
    -- The command that starts the language server
    -- See: https://github.com/eclipse/eclipse.jdt.ls#running-from-the-command-line
    cmd = {
      home .. "/.local/share/nvim/mason/bin/jdtls",
      "-data",
      workspace_dir,
    },
    -- This is the default if not provided, you can remove it. Or adjust as needed.
    -- One dedicated LSP server & client will be started per unique root_dir
    root_dir = root_dir,
    -- Here you can configure eclipse.jdt.ls specific settings
    -- See https://github.com/eclipse/eclipse.jdt.ls/wiki/Running-the-JAVA-LS-server-from-the-command-line#initialize-request
    -- for a list of options
    settings = {
      java = {},
    },
    handlers = {
      ["language/status"] = function()
      end
    },
    init_options = {
      bundles = get_jdtls_bundles()
    }
  }
  jdtls.start_or_attach(config)
end

java_callback()
